#! /usr/bin/env python3


import argparse
import csv
import html
import os
import sys

import lxml.etree
import lxml.html
import requests


cookie = ''


def column_to_text(column, amendement, amendement_complet):
    value = (amendement[column['refDataField']] if 'refDataField' in column
        else amendement_complet[column['refDataFieldComplet']])
    if column.get('isHtml', False):
        value = html_to_text(value)
    elif not value:
        value = ''
    else:
        options = column['editTypeSelectOptionList']
        if options:
            for option in options:
                if value == option['value']:
                    value = option['label']
                    break
    return value


def html_to_text(s):
    try:
        s = lxml.html.tostring(lxml.html.fromstring(html.unescape(s)), method='text', encoding=str)
    except lxml.etree.XMLSyntaxError:
        s = ""
    s = s.strip()
    while '\n\n\n' in s:
        s.replace('\n\n\n', '\n\n')
    return s


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-m',
        '--max',
        help="nombre maximum d'amendements à récupérer",
        type=int,
        )
    parser.add_argument(
        '-n',
        '--numero',
        help='numéro Bibard',
        )
    parser.add_argument(
        'target',
        help='Répertoire où stocker',
        )
    args = parser.parse_args()

    response = requests.get(
        'https://eloi-ext.assemblee-nationale.fr/eloi/tableauDeBord/affichage.sp?page=1&start=0&limit=25',
        headers={'Cookie': cookie},
        )
    response.raise_for_status()
    response_json = response.json()
    examens = response_json['data']['examenEtats']

    if not args.numero:
        for examen in examens:
            if examen['instance'] != 'Séance':
                continue
            print(
                examen['bibard'],
                examen['labelTexte'],
                )
        return 0

    for examen in examens:
        if examen['bibard'] == args.numero:
            break

    # Note: The call to chargerHeaders must be done before calling gestionAmendements.
    response = requests.get(
        'https://eloi-ext.assemblee-nationale.fr/eloi/grille/chargerHeaders.sp?examenId={}&mode=mode'.format(
            examen['id'],
            ),
        headers={'Cookie': cookie},
        )
    response.raise_for_status()
    response_json = response.json()
    columns = response_json['data']['columns']
    columns.append({
        'editTypeSelectOptionList': [],
        'isHtml': True,
        'label': 'Exposé sommaire',
        'refDataFieldComplet': 'amendementExposeSommaire',
        'showToolTip': True,
        })

    response = requests.get(
        'https://eloi-ext.assemblee-nationale.fr/eloi/grille/gestionAmendements.sp?page=1&start=0&limit=9999'
        '&sort=[{"property":"amendementOrdreDePassage","direction":"ASC"}]'
        '&filter=[{"operator":"in","property":"amendementEtat","value":["EC","AT","T","ER","R","AC","DI"]}]',
        headers={'Cookie': cookie},
        )
    response.raise_for_status()
    response_json = response.json()
    records = response_json['read2']['records']
    with open(os.path.join(args.target, 'amendements-{}.csv'.format(examen['bibard'])), 'w') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow([
            html_to_text(column['label'])
            for column in columns
            if column['showToolTip']
            ])
        for index, amendement in enumerate(records):
            if args.max is not None and index >= args.max:
                break
            print("Récupération de l'amendement {}".format(amendement['id']))
            response = requests.get(
                'https://eloi-ext.assemblee-nationale.fr/eloi/grille/apercuAmendement.sp?amdtId={}&mode=NORMAL'.format(
                    amendement['id']),
                headers={'Cookie': cookie},
                )
            response.raise_for_status()
            response_json = response.json()
            amendement_complet = response_json['data']
            csv_writer.writerow([
                column_to_text(column, amendement, amendement_complet)
                for column in columns
                if column['showToolTip']
                ])

    return 0


if __name__ == '__main__':
    sys.exit(main())
